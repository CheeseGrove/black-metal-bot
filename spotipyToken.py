#!usr/bin/python

from __future__ import print_function
import spotipy
import spotipy.util as util

#Added to config, remove if working
#USERNAME = 'zit1432'
#SPOTIPY_CLIENT_ID = '62c32fd769a24b4d80f7137ce26f776e'
#SPOTIPY_CLIENT_SECRET = '2444044552f44837b1d3d8ecae905b53'
SPOTIPY_REDIRECT_URI = 'http://localhost/'
SCOPE = 'playlist-modify-public'



def getSpotipyToken(username, spotipyClientId, spotipyClientSecret) :


    token = util.prompt_for_user_token(username,SCOPE,client_id=spotipyClientId,client_secret=spotipyClientSecret,redirect_uri=SPOTIPY_REDIRECT_URI)

    if token:
        sp = spotipy.Spotify(auth=token)
        return token
    else:
        print("Can't get token for", username)

