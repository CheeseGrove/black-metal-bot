
from reddit import getRedditSongs
from spotipyToken import getSpotipyToken
from spotify import getListFromPlaylist, searchSong, getToken, addToPlaylist, checkAgeOfSongs, removeSongs
from time import sleep
from configParser import readConfig


def addSongs(playlist, redditList, access_token, userToken) :
    spotifyList = getListFromPlaylist(playlist, userToken)

    #JÄMNFÖR SPOTIFY LISTAN MED DE SOM SPOTIFY SEARCH RETURNERAR ISTÄLLET


    #Can be more efficient
    for track in spotifyList:
        track['song'] = track.get('song').lower()
        track['artist'] = track.get('artist').lower()

    for track in redditList:
        track['song'] = track.get('song').lower()
        track['artist'] = track.get('artist').lower()


    lessDupesList = [x for x in redditList if x not in spotifyList]

    listToSend = []
    for track in lessDupesList :
        uri = searchSong(token, track.get('song'), track.get('artist'), spotifyList)
        listToSend.append(uri)

    listToSend = [x for x in listToSend if x is not None]

    if listToSend :
        addToPlaylist(listToSend, playlist, userToken)



config = readConfig()

playlistPersistent = config['spotify']['playlistpersistentid']
playlistWeekly = config['spotify']['playlistweeklyid']
timeSleeping = int(config['misc']['timesleeping'])


#MAIN LOOP
while True :
    userToken = getSpotipyToken(config['spotipy']['username'], config['spotipy']['clientid'], config['spotipy']['clientsecret'])

    #Explain what auth is in readme, maybe remove if spotipy works for all
    token = getToken(config['spotify']['auth'])

    redditList = getRedditSongs(config['reddit']['subreddit'],
                    int(config['reddit']['numberofposts']),
                    config['reddit']['redditid'],
                    config['reddit']['redditsecret'],
                    config['reddit']['username'],
                    config['reddit']['password'],
                    config['reddit']['useragent'])

    addSongs(playlistWeekly, redditList, token, userToken)
    addSongs(playlistPersistent, redditList, token, userToken)

    toRemove = checkAgeOfSongs(playlistWeekly, userToken)
    if toRemove:
        removeSongs(toRemove, userToken, playlistWeekly)

    print("Sleeping for " + str(timeSleeping) + " seconds..")
    sleep(timeSleeping)

