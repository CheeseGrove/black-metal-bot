#!usr/bin/python

import requests
import base64
import json
from spotipyToken import getSpotipyToken
from datetime import datetime, timedelta




def getToken(auth) :

    #Add base64 parser
    #base64Auth = 'NjJjMzJmZDc2OWEyNGI0ZDgwZjcxMzdjZTI2Zjc3NmU6MjQ0NDA0NDU1MmY0NDgzN2IxZDNkOGVjYWU5MDViNTM='

    spotify_token_url = "https://accounts.spotify.com/api/token"

    headers = { 'Authorization': 'Basic ' + auth }

    param = {
        'grant_type': 'client_credentials',
        'scope': ['playlist-modify-public', 'playlist-modify-private']
    }


    req = requests.post(spotify_token_url, data = param, headers=headers)
    if req.status_code == 200:
        access_token = req.json().get('access_token')

        return access_token


def getListFromPlaylist(playlistID, userToken) :

    uri = 'https://api.spotify.com/v1/playlists/' + playlistID + '/tracks'

    headers = {
        'Authorization': 'Bearer ' + userToken
    }

    resp = requests.get(uri, headers = headers)

    if resp.status_code == 200 :
        data = resp.json()

        items = data['items']

        artist = ''
        song = ''

        list = []

        for item in items :
            track = item['track']
            artist = track['artists'][0]['name']
            song = track['name']
            item = { "artist": artist, "song": song }
            list.append(item)


        return list




def checkAgeOfSongs(playlistID, userToken) :
    uri = 'https://api.spotify.com/v1/playlists/' + playlistID + '/tracks'

    headers = {
        'Authorization': 'Bearer ' + userToken
    }

    resp = requests.get(uri, headers = headers)

    if resp.status_code == 200 :
        data = resp.json()

        items = data['items']



        list = []

        for item in items :
            added = item['added_at']

            #no one likes dates

            day = added[:10]
            day = day[8:]
            month = added[:7]
            month = month[5:]
            year = added[:4]

            if ((datetime(int(year), int(month), int(day)) - (datetime.now() - timedelta(days=7))) < timedelta(days=0)) :



                track = item['track']

                print('Remove: ' + track['name'])

                uri = track['uri']
                list.append(uri)

        return list


def removeSongs(uri, userToken, playlistID) :
    body = {}

    listOfURIS = []
    for track in uri :


        track = track.replace(" ", "%20")

        uri = "https://api.spotify.com/v1/playlists/" + playlistID + "/tracks"

        listOfURIS.append(track)

        body = {
            "uris": listOfURIS
        }


    headers = {
        'Authorization': 'Bearer ' + userToken,
        'Content-Type': 'application/json'
    }

    resp = requests.delete(uri, data = json.dumps(body), headers = headers)
    if resp.status_code == 20 :
        print('Successfully removed song')
    else :
        print('Unsuccessfully removed song')



def searchSong(access_token, song, artist, dupesList) :

    song = song.replace(" ", "%20")
    artist = artist.replace(" ", "%20")

    searchURL = 'https://api.spotify.com/v1/search?q=track: ' + song + '%20artist: ' + artist + '&type=track&limit=1'

    headers = {
        'Authorization': 'Bearer ' + access_token
    }

    get = requests.get(searchURL, headers = headers)

    if get.status_code == 200 :
        try :
            data = get.json()

            firstEntry = data['tracks']['items'][0]
            bandName = firstEntry['artists'][0]['name']
            songName = firstEntry['name']
            for track in dupesList :
                if(track['song'] == songName.lower() and track['artist'] == bandName.lower()) :
                    print('[Warning] Duplicate found.. Song: ' + songName + ', Band: ' + bandName)
                    return


            uri = firstEntry['uri']

            print("Found Song: " + firstEntry['name'] + ' By: ' + firstEntry['artists'][0]['name'])

            return uri

        except IndexError:
            print('[!!!] ' + artist.replace('%20', ' ') + " - " + song.replace('%20', ' ') + "Song not found..")


def addToPlaylist(uri, playlistID, userToken) :

    body = {}

    listOfURIS = []
    for track in uri :


        track = track.replace(" ", "%20")

        uri = "https://api.spotify.com/v1/playlists/" + playlistID + "/tracks"

        listOfURIS.append(track)

        body = {
            "uris": listOfURIS
        }


    headers = {
        'Authorization': 'Bearer ' + userToken,
        'Content-Type': 'application/json'
    }

    resp = requests.post(uri, data = json.dumps(body), headers = headers)
    if resp.status_code == 201 :
        print('Successfully added song')
    else :
        print('Unsuccessfully added song')





#token = getToken()
#uri = searchSong(token)

#playlistPersistent = "7IpYTb8SrUHJ7GRfZmavC6"
#playlistWeekly = "4d1Vw2XnnQv6kqpYkr71gp"

#userToken = getSpotipyToken()

#checkAgeOfSongs(playlistWeekly, userToken)

#getListFromPlaylist(playlistWeekly, userToken)

#addToPlaylist([uri], playlistPersistent, userToken)
#addToPlaylist([uri], playlistWeekly, userToken)



