from configparser import ConfigParser

def readConfig() :

    try :
        file = 'config.ini'
        config = ConfigParser()
        config.read(file)

        #Returns dict created from config file

        dictionary = {}
        for section in config.sections():
            dictionary[section] = {}
            for option in config.options(section):
                dictionary[section][option] = config.get(section, option)

        return dictionary

    except :
        print('Unable to parse config')
        exit()





