#!usr/bin/python

import praw

def getRedditSongs(subreddit, limit, redditId, redditSecret, username, password, userAgent) :

    #Added to config, remove if working
    #reddit_id = "N5GjdHDcdcYXgg"
    #reddit_secret = "uSQOK2gQfztwxT3YD34MedpMGdBlcQ"

    reddit = praw.Reddit(client_id = redditId,
                         client_secret = redditSecret,
                         username = username,
                         password = password,
                         user_agent = userAgent)



    subreddit = reddit.subreddit(subreddit)

    new = subreddit.new(limit=limit)

    list = []

    for post in new:

        #IGNORE THIS, ITS HORRIBLE
        try :

            title = post.title
            artist = None
            song = None
            if "-" in title :
                artist = title.split("-", 1)[0]
                if "]" in artist :
                    artist = artist.split("]", 1)[1]
                if ")" in artist :
                    artist = artist.split(")", 1)[1]
                if "}" in artist :
                    artist = artist.split("}", 1)[1]
                if artist.startswith(" ") :
                    artist = song[1:]
                if artist.endswith(" ") :
                    artist = artist[:(len(artist) - 1)]

            if "-" in title :
                song = title.split("-", 1)[1]
                if "(" in song :
                    song = song.split("(", 1)[0]
                if "[" in song :
                    song = song.split("[", 1)[0]
                if "{" in song :
                    song = song.split("{", 1)[0]
                if song.startswith(" ") :
                    song = song[1:]
                if song.endswith(" ") :
                    song = song[:(len(song) - 1)]

        except :
            print("Format error for: " + title)


        if artist is not None and song is not None :
            item = { "artist": artist, "song": song }
            list.append(item)

    return list
